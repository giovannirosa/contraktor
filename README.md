# Contraktor

Projeto feito para testar minhas habilidades com React.

## Arquitetura

Projeto altamente simplificado onde foi utilizado [Firebase](https://firebase.google.com/) como banco de dados e [Material UI](https://material-ui.com/) para componentes frontend.

## Instruções de Instalação

Para executar o código faça:
- Instale o [NodeJS](https://nodejs.org/en/)
- Clone este repositório
- Execute yarn install
- Execute yarn start

## Instruções de Uso

Basicamente são três CRUD simples:
- Contratos: lista de contratos disponíveis
- Partes: lista de partes disponíveis
- Relações: lista de relações disponíveis, é possível relacionar um contrato com uma ou mais pessoas

## Bugs

Pela simplicidade várias validações não estão sendo feitas, sendo possível que haja alguns bugs durante o funcionamento.

## Autor

- Giovanni Rosa - [giovannirosa](https://github.com/giovannirosa)