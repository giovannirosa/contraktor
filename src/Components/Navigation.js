import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { Description, Person, AssignmentInd } from '@material-ui/icons';
import Contracts from './Contracts';
import Parts from './Parts';
import { Paper } from '@material-ui/core';
import Relations from './Relations';

const useStyles = makeStyles(theme => ({
  navigation: {
    width: '100%',
    position: 'fixed',
    bottom: 0,
    backgroundColor: theme.palette.primary.main
  },
  selected: {
    color: '#FFF',
    label: {
      color: '#FFF',
    }
  }
}));

export default function Navigation() {
  const classes = useStyles();
  const [value, setValue] = React.useState('contracts');

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  let content = null;
  if (value === 'contracts') {
    content = <Contracts />;
  } else if (value === 'parts') {
    content = <Parts />;
  } else if (value === 'relations') {
    content = <Relations />;
  }

  return (
    <div>
      {content}
      <Paper>
        <BottomNavigation value={value} onChange={handleChange} className={classes.navigation}>
          <BottomNavigationAction
            label="Contracts"
            value="contracts"
            icon={<Description />} />
          <BottomNavigationAction
            label="Parts"
            value="parts"
            icon={<Person />} />
          <BottomNavigationAction
            label="Relations"
            value="relations"
            icon={<AssignmentInd />} />
        </BottomNavigation>
      </Paper>
    </div>
  );
}