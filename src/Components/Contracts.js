import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import { Typography, Paper, TextField, Grid, Button, CircularProgress } from '@material-ui/core';
import { DatePicker } from "@material-ui/pickers";
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import 'filepond/dist/filepond.min.css';
import MUIDataTable from "mui-datatables";
import FirebaseService from '../services/FirebaseService';

const styles = {
  root: {
    padding: 20,
    height: '100%'
    // width: '100%',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 10
  },
  table: {
    paddingTop: 10
  }
};

registerPlugin(FilePondPluginFileValidateType);

// const permitedExtensions = ['xlsx', 'xls', 'csv'];

const acceptedFileTypes = [
  'application/pdf',
  'application/msword',
]

const fileValidateTypeLabelExpectedTypesMap = {
  'application/pdf': '.pdf',
  'application/msword': '.doc',
}

class Contracts extends Component {
  state = {
    files: [],
    title: '',
    startDate: null,
    endDate: null,
    data: [],
    loading: false
  };

  componentDidMount() {
    setTimeout(() => {
      this.ref.focus();
    }, 100);
    this.setState({ loading: true });
    FirebaseService.getDataList('contracts',
      (dataReceived) => this.setState({ data: dataReceived, loading: false }))
  }

  addHandler = (event) => {
    event.preventDefault();

    const { files, title, startDate, endDate } = this.state;
    const newid = FirebaseService.pushData('contracts', {
      document: files.length > 0 ? files[0].name : null,
      title,
      startDate: startDate ? startDate.toISOString() : null,
      endDate: endDate ? endDate.toISOString() : null
    });
    console.log(newid);
    this.cancelHandler();
  };

  handleChange = (prop) => event => {
    let val = event;
    if (prop === 'title') {
      val = event.target.value;
    }
    console.log(val);
    this.setState({ [prop]: val });
  }

  cancelHandler = () => {
    this.setState({ files: [], title: '', startDate: null, endDate: null });
    this.ref.focus();
  }

  onRowsDelete = (rowsDeleted, dataRows) => {
    console.log(rowsDeleted, dataRows);
    const { data } = this.state;
    rowsDeleted.data.forEach(d => FirebaseService.remove(data[d.dataIndex].key, 'contracts'));
  }

  render() {
    const { classes } = this.props;
    const { files, title, startDate, endDate, data, loading } = this.state;

    console.log('DATA', data);

    const transformedData = data.map(d =>
      [
        d.title,
        d.startDate ? new Date(d.startDate).toLocaleDateString('pt-BR') : '',
        d.endDate ? new Date(d.endDate).toLocaleDateString('pt-BR') : '',
        d.document
      ]
    );

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <div><Typography variant='h3' color='textSecondary'>{'Contracts'}</Typography></div>
          <div>
            <TextField
              inputRef={ref => this.ref = ref}
              style={{ marginRight: 10 }}
              label='Title'
              value={title}
              margin='normal'
              onChange={this.handleChange('title')} />
            <DatePicker
              style={{ marginRight: 10 }}
              clearable
              label={'Start Date'}
              value={startDate}
              format="dd/MM/yyyy"
              margin="normal"
              onChange={this.handleChange('startDate')}
              maxDate={endDate ? endDate : null} />
            <DatePicker
              clearable
              label={'End Date'}
              value={endDate}
              format="dd/MM/yyyy"
              margin="normal"
              onChange={this.handleChange('endDate')}
              minDate={startDate ? startDate : null} />
            <div style={{ paddingTop: 10 }}>
              <FilePond
                ref={ref => (this.pond = ref)}
                files={files}
                onupdatefiles={fileItems => {
                  // Set currently active file objects to this.state
                  this.setState({
                    files: fileItems.map(fileItem => fileItem.file)
                  });
                }}
                fileValidateTypeLabelExpectedTypesMap={fileValidateTypeLabelExpectedTypesMap}
                acceptedFileTypes={acceptedFileTypes} />
            </div>
            <Grid container justify="flex-end" spacing={2}>
              <Grid item>
                <Button
                  onClick={this.addHandler}
                  variant="contained"
                  color="secondary">
                  {'Add'}
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={this.cancelHandler}
                  variant="contained"
                  color="primary">
                  {'Cancel'}
                </Button>
              </Grid>
            </Grid>
          </div>
          <div className={classes.table}>
            {loading ?
              <CircularProgress /> :
              <MUIDataTable
                title={""}
                data={transformedData}
                columns={['Title', 'Start Date', 'End Date', 'Document']}
                options={{ onRowsDelete: this.onRowsDelete }} />}
          </div>
        </Paper>
      </div>
    );
  }
};

export default withStyles(styles)(Contracts);