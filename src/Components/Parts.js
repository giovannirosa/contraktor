import React, { Component } from 'react';
import { Paper, Typography, TextField, Grid, Button, withStyles, CircularProgress } from '@material-ui/core';
import MUIDataTable from "mui-datatables";
import FirebaseService from '../services/FirebaseService';

const styles = {
  root: {
    padding: 20,
    height: '100%'
    // width: '100%',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 10
  },
  table: {
    paddingTop: 10
  }
};

class Parts extends Component {
  state = {
    name: '',
    surname: '',
    email: '',
    cpf: '',
    phone: '',
    data: [],
    loading: false
  };

  componentDidMount() {
    setTimeout(() => {
      this.ref.focus();
    }, 100);
    this.setState({ loading: true });
    FirebaseService.getDataList('parts',
      (dataReceived) => this.setState({ data: dataReceived, loading: false }))
  }

  addHandler = (event) => {
    event.preventDefault();

    const { name, surname, email, cpf, phone } = this.state;
    const newid = FirebaseService.pushData('parts', {
      name,
      surname,
      email,
      cpf,
      phone
    });
    console.log(newid);
    this.cancelHandler();
  };

  handleChange = (prop) => event => {
    let val = event.target.value;
    this.setState({ [prop]: val });
  }

  cancelHandler = () => {
    this.setState({
      name: '',
      surname: '',
      email: '',
      cpf: '',
      phone: ''
    });
    this.ref.focus();
  }

  onRowsDelete = (rowsDeleted, dataRows) => {
    console.log(rowsDeleted, dataRows);
    const { data } = this.state;
    rowsDeleted.data.forEach(d => FirebaseService.remove(data[d.dataIndex].key, 'parts'));
  }

  render() {
    const { classes } = this.props;
    const { name, surname, email, cpf, phone, data, loading } = this.state;

    console.log('DATA', data);

    const transformedData = data.map(d =>
      [
        d.name,
        d.surname,
        d.email,
        d.cpf,
        d.phone
      ]
    );

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <div>
            <Typography variant='h3' color='textSecondary'>{'Parts'}</Typography>
          </div>
          <div>
            <div>
              <TextField
                inputRef={ref => this.ref = ref}
                style={{ marginRight: 10 }}
                label='Name'
                value={name}
                margin='normal'
                onChange={this.handleChange('name')} />
              <TextField
                style={{ marginRight: 10 }}
                label='Surname'
                value={surname}
                margin='normal'
                onChange={this.handleChange('surname')} />
            </div>
            <div>
              <TextField
                style={{ marginRight: 10 }}
                label='Email'
                value={email}
                margin='normal'
                onChange={this.handleChange('email')} />
              <TextField
                style={{ marginRight: 10 }}
                label='CPF'
                value={cpf}
                margin='normal'
                onChange={this.handleChange('cpf')} />
            </div>
            <TextField
              style={{ marginRight: 10 }}
              label='Phone'
              value={phone}
              margin='normal'
              fullWidth
              onChange={this.handleChange('phone')} />
            <Grid container justify="flex-end" spacing={2}>
              <Grid item>
                <Button
                  onClick={this.addHandler}
                  variant="contained"
                  color="secondary">
                  {'Add'}
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={this.cancelHandler}
                  variant="contained"
                  color="primary">
                  {'Cancel'}
                </Button>
              </Grid>
            </Grid>
          </div>
          <div className={classes.table}>
            {loading ?
              <CircularProgress /> :
              <MUIDataTable
                title={""}
                data={transformedData}
                columns={['Name', 'Surname', 'Email', 'CPF', 'Phone']}
                options={{ onRowsDelete: this.onRowsDelete }} />}
          </div>
        </Paper>
      </div>
    );
  }
};

export default withStyles(styles)(Parts);