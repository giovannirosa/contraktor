import React, { Component } from 'react';
import { Paper, Typography, Grid, Button, withStyles, CircularProgress, ListItem, ListItemIcon, Checkbox, ListItemText, List, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import MUIDataTable from "mui-datatables";
import FirebaseService from '../services/FirebaseService';

const styles = theme => ({
  root: {
    padding: 20,
    height: '100%'
    // width: '100%',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 10
  },
  table: {
    paddingTop: 10
  },
  list: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
});

class Parts extends Component {
  state = {
    parts: [],
    loading: false,
    data: [],
    contracts: [],
    selectedContract: ''
  };

  componentDidMount() {
    setTimeout(() => {
      this.ref && this.ref.focus();
    }, 100);
    this.setState({ loading: true });

    FirebaseService.getDataList('relations',
      (data) =>
        FirebaseService.getDataList('contracts',
          (conts) =>
            FirebaseService.getDataList('parts',
              (parts) => this.setState({
                parts: parts.map(p => ({ ...p, checked: false })),
                contracts: conts,
                loading: false,
                data: data
              }))));
  }

  handleToggle = key => () => {
    this.setState((prevState) => {
      const updatedParts = Array.of(...prevState.parts);
      const item = updatedParts.find(p => p.key === key);
      item.checked = !item.checked;
      return { parts: updatedParts };
    });
  };

  handleChange = (prop) => event => {
    this.setState({ [prop]: event.target.value });
  }

  addHandler = (event) => {
    event.preventDefault();

    const { selectedContract, parts } = this.state;
    const newid = FirebaseService.pushData('relations', {
      contract: selectedContract,
      parts: parts.filter(p => p.checked).map(p => p.key)
    });
    console.log(newid);
    this.cancelHandler();
  };

  onRowsDelete = (rowsDeleted, dataRows) => {
    console.log(rowsDeleted, dataRows);
    const { data } = this.state;
    rowsDeleted.data.forEach(d => FirebaseService.remove(data[d.dataIndex].key, 'relations'));
  }

  cancelHandler = () => {
    this.setState((prevState) => {
      const updatedParts = prevState.parts.map(p => ({
        ...p, checked: false
      }));
      return { parts: updatedParts, selectedContract: '' };
    });
    this.ref.focus();
  }

  render() {
    const { classes } = this.props;
    const { data, loading, selectedContract, contracts, parts } = this.state;

    console.log('DATA', data);

    const transformedData = data.map(d => {
      const cont = contracts.find(c => c.key === d.contract);
      const pts = d.parts.map(a => {
        const pt = parts.find(c => c.key === a);
        return pt ? pt.name : '';
      }).reduce((ret, v) => {
        let str = ret;
        if (ret !== '') {
          str += ', ';
        }
        return str + v;
      });
      return [
        cont ? cont.title : '',
        pts
      ];
    });

    const focusInputField = input => {
      // if (input && input.node) {
      //   setTimeout(() => { input && input.focus() }, 100);
      // }
      this.ref = input;
    };

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <div>
            <Typography variant='h3' color='textSecondary'>{'Relations'}</Typography>
          </div>
          {loading ?
            <div style={{ paddingTop: 10 }}>
              <CircularProgress />
            </div> :
            <div>
              <div>
                <FormControl
                  margin={'normal'}
                  style={{ width: '100%' }}>
                  <InputLabel>{'Contract'}</InputLabel>
                  <Select
                    inputRef={focusInputField}
                    value={selectedContract}
                    onChange={this.handleChange('selectedContract')}>
                    {contracts.map(opt =>
                      <MenuItem
                        key={opt.key}
                        value={opt.key}>
                        {opt.title}
                      </MenuItem>)}
                  </Select>
                </FormControl>
                <List className={classes.list}>
                  {parts.map(value => {
                    const labelId = `checkbox-list-label-${value.key}`;
                    return (
                      <ListItem
                        key={value.key}
                        role={undefined}
                        dense
                        button
                        onClick={this.handleToggle(value.key)}>
                        <ListItemIcon>
                          <Checkbox
                            edge="start"
                            checked={value.checked}
                            tabIndex={-1}
                            disableRipple
                            inputProps={{ 'aria-labelledby': labelId }} />
                        </ListItemIcon>
                        <ListItemText id={labelId} primary={value.name} />
                      </ListItem>
                    );
                  })}
                </List>
                <Grid container justify="flex-end" spacing={2}>
                  <Grid item>
                    <Button
                      onClick={this.addHandler}
                      variant="contained"
                      color="secondary">
                      {'Add'}
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      onClick={this.cancelHandler}
                      variant="contained"
                      color="primary">
                      {'Cancel'}
                    </Button>
                  </Grid>
                </Grid>
              </div>
              <div className={classes.table}>
                {loading ?
                  <CircularProgress /> :
                  <MUIDataTable
                    title={""}
                    data={transformedData}
                    columns={['Contract', 'Parts']}
                    options={{ onRowsDelete: this.onRowsDelete }} />}
              </div>
            </div>}
        </Paper>
      </div>
    );
  }
};

export default withStyles(styles)(Parts);