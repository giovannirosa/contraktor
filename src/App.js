import React from 'react';
import './App.css';
import { AppBar, Toolbar, createMuiTheme, MuiThemeProvider, Typography } from '@material-ui/core';
import Navigation from './Components/Navigation';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const theme = createMuiTheme({
  overrides: {
    MuiBottomNavigationAction: {
      root: {
        color: 'white',
        '&$selected': {
          paddingTop: 6,
          color: 'white',
        },
      },
      label: {
        color: 'white',
        '&$selected': {
          color: 'white',
        },
      }
    }
  }
});

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <React.Fragment>
          <AppBar position="fixed">
            <Toolbar style={{ display: 'flex', justifyContent: 'center' }}>
              <Typography variant="h4" type="title" color="inherit">
                {'Contraktor'}
              </Typography>
            </Toolbar>
          </AppBar>
          <div className="App">
            <Navigation />
          </div>
        </React.Fragment>
      </MuiPickersUtilsProvider>
    </MuiThemeProvider>
  );
}

export default App;
