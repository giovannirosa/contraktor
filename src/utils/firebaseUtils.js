import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyB9uAdBTH1Uzr0BNgbWXrcGoSksl3IX5PY",
  authDomain: "contraktor-6ffb6.firebaseapp.com",
  databaseURL: "https://contraktor-6ffb6.firebaseio.com",
  projectId: "contraktor-6ffb6",
  storageBucket: "",
  messagingSenderId: "962085318937",
  appId: "1:962085318937:web:e044bb4ef5c3e58c"
};

export const firebaseImpl = firebase.initializeApp(config);
export const firebaseDatabase = firebase.database();